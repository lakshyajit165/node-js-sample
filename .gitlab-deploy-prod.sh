#!/bin/bash

# get servers list
set -f
string=$PROD_DEPLOY_SERVER

array=(${string//,/ })

# Iterate servers for deploy, pull last commit, stop the containers, remove images, rebuild images and start the container again.
for i in "${!array[@]}"; do
    echo "Deploy project on server ${array[i]}"
    ssh ubuntu@${array[i]} "cd node-js-sample \
    && git pull origin master \
    && sudo docker stop nodecontainer \
    && sudo docker rm nodecontainer \
    && sudo docker rmi lakshyajit165/nodeimage \
    && sudo docker build -t lakshyajit165/nodeimage . \
    && sudo docker run -d --name nodecontainer -p 5000:5000 lakshyajit165/nodeimage"
done